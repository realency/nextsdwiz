Offline zxdb-downloader archive folder
======================================

Drop zxdb-downloader archives in this folder to make them available offline.

Images may take two forms:

1. **Zip archives**. Each archive MUST be in the zip file format and MUST have a `.zip` file extension.  The zip file should contain folders `dot` and `zxdb-dl` as included in the standard archive for that application.
2. **Folders**. The folder may have any name and should directly contain the `dot` and `zxdb-dl` folders.

Archives provided in this way should appear in the zxdb-dl menu page of the wizard.

The official image at the time of writing is v1.3.2 and can be downloaded [here](http://www.zxspectrumnext.online/sn-complete-v.1.3.2.zip).

A better option, however, may be to get a bang-up-to-date image directly from the GitLab repository.  You can get that [here](https://gitlab.com/thesmog358/tbblue/-/archive/master/tbblue-master.zip).
