Overlays folder
===============

Overlays are folder and file hierarchies that are copied over the top of the SD image filesystem.  The file system hierarchy in an overlay component is copied faithfully to the image without the wizard requiring native understanding of what the component contains.

The contents of this folder are structured into:

1. A folder for each overlay.
2. Within that folder, one or more components that can be optionally included.  Each component may be either a zip file with a `.zip` extension, or a folder.

Each overlay defined this way is represented as a separate page in the wizard.  

Each component within the overlay is represented as an option in the menu for that overlay.

If selected, the folder structure within each overlay component is copied to the root of the SD image.

Example
-------

Suppose the following folders and files:

``` Text
./overlays/
    ¦_ Games/
        ¦_ 48K Favourites/
        ¦   ¦_ games/
        ¦       ¦_ Classic48/
        ¦           ¦_ Atic Atac.tzx
        ¦           ¦_ Jetpac.tzx
        ¦           ¦_ Manic Miner.tap
        ¦_ Rusty Pixels.zip


```

This will result in the addition of a page to the wizard with the heading `Select Games packages`.  The page will present two options that can each be toggled on and off:

* `48K Favourites`
* `Rusty Pixels`

Selecting `48K Favourites` will result in the addition of those three games files to the `games/Classic48` folder on your SD image.

Meanwhile selecting `Rusty Pixels` will result in the `Rusty Pixels.zip` file being unpacked to the root of the SD image, thus using the folder structure in the compressed archive.
