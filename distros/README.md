Offline ZX Next SD image folder
===============================

Drop ZX Next SD images in this folder to make them available offline.

Images may take two forms:

1. **Zip archives**. Each archive MUST be in the zip file format and MUST have a `.zip` file extension.  The zip file should directly contain the Next SD file system **OR** should contain a single folder that directly contains the Next file system.  The latter option accommodates archives downloaded directly from GitLab.
2. **Folders**. The folder may have any name and should directly contain the Next SD file system **OR** should contain a single folder that directly contains the Next file system.

Archives provided in this way should appear in the Distro menu page of the wizard.

The official image at the time of writing is v1.3.2 and can be downloaded [here](http://www.zxspectrumnext.online/sn-complete-v.1.3.2.zip).

A better option, however, is to get a bang-up-to-date image directly from the GitLab repository.  You can get that [here](https://gitlab.com/thesmog358/tbblue/-/archive/master/tbblue-master.zip).
