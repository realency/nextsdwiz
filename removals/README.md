Removals folder
===============

You may want to delete some of the files from your image.  For example, Next SD images downloaded from GitLab contain a `.gitignore` file at the root that is extraneous.  The standard image also contains many sample and demo files.  These are awesome for learning about some of the Next's capabilities but may be seen as clutter once those capabilities have been well understood.

Define bundles of files to be deleted by listing them in plain text files in this folder.

Each file should:

* Be given a meaningful name.  The name of the file will be used as the display name in the wizard menu.
* Not be named `.DS_Store` or `README.md`.  For obvious reasons.
* Be encoded as UTF-8.  If you're using a standard text editor, this is likely to be the default encoding used.
* Simply list the folders and files to be deleted.  Any folder listed will be deleted in its entirity, including any hierarchies of sub-folders and files.

Any files or folders referenced that do not exist will be silently ignored.

Example
-------

A file called `Demo Music` containing the following

``` text
demos/OnlyYou6ch
demos/NextDAW/SongProjects
```

will result in an option in the Removals page of the wizard labeled 'Demo Music'.  Selecting the option will delete the two folders listed from your image.
