Next SD Wizard
==============

A terminal-based wizard for preparing SD cards for the ZX Spectrum Next.

* Unpacks archives from a number of possible sources includng offline zip files, and online download locations.
* Natively knows about download locations for the Next SD image, CP/M, and zxdb-downloader.
* Removes unwanted files.
* Overlays additional folder and file hierarchies for custom packages such as user backups and game packages.
* Builds an image to a customisable location.

Runs as a Python script under Python 3.10 or later.

Prerequisites
-------------

**You'll need a basic understanding of Python**.  How to install python modules and how to run python scripts.  There is no installer provided.

**Requires Python 3.10**.  Or later.  If you have multiple Python installations on your system, ensure you are using the correct one.  Determine your python version using the `--version` argument.  For example, on your system you might type one of:

* `python --version`
* `python3 --version`
* `py --version`

The latest version of Python is available [here](https://www.python.org/downloads/).

**Certificates**.  In order to download from the internet, Python needs to have standard certificates installed.  On some systems, this is not an automatic part of the Python installation, but is easily achieved.  For example, on MacOS, you will likely need to run the `Install Certificates.command` macro provided with your Python installation.  More info [here](https://stackoverflow.com/questions/40684543/how-to-make-python-use-ca-certificates-from-mac-os-truststore).

**Required Modules**.  On many systems, all the Python modules used are built in to the Python distribution.  On Windows systems the required `curses` module does not exist. A Windows port of the module can be installed using pip in the usual way.  The windows port is called `windows-curses` and installing it is likely to look something like this:

``` DOS
py -m pip install windows-curses
```

More information available [here](https://pypi.org/project/windows-curses/).

Using the Wizard
----------------

Run the Wizard under Python however you normally do that.  For example, at a command line you might type something like this from the root folder of this project:

``` DOS
python3 nextsdwiz.py
```

The user interface should be fairly self-explanatory and, I believe, need no instructions here.

Customising the Wizard
----------------------

The main way to customise the wizard is by dropping files into pre-defined folders that live under the root folder of this project.  Each of these folders already exists in this project and has its own README with further explanations of usage.

* You can download Next SD Card images ("distributions" or "distros") and make them available to the wizard offline by dropping them in the `./distros/` folder.  See [this README](distros/README.md).
* You can download CP/M archives and make them available to the wizard offline by dropping them in the `./cpm/` folder.  See [this README](cpm/README.md).
* You can download zxdb-downloader archives and make them available to the wizard offline by dropping them in the `./zxdb-dl/` folder.  See [this README](zxdb-dl/README.md).
* You can specify lists of files to be optionally deleted from the image in a simple text format in the `./removals/` folder.  See [this README](removals/README.md).
* You can specify custom file and folder structures to be optionally overlaid onto the image.  These can be dropped in the `./overlays/` folder, using a specific folder strucutre.  See [this README](overlays/README.md)

You can also customise the code itself.  Many of the things you might want to cutsomise, including the menu structures presented, are captured as 'constants' in conspicuous places in the `*.py` files.

Known Issues
------------

1. On Windows, the wizard does not behave well when the terminal window is resized.
2. Long status message lines and error message lines wrap onto next line and trailing text is not properly cleaned up.

Disclaimers
-----------

This Wizard was written for my own personal use and is shared with the expectation that others accept it as such.  A couple of caveats in particular:

1. The Wizard does nothing particularly clever.  It downloads files, unzips archives, and moves some files around on your file system.  As such, I can't see any way for this Wizard to cause any damage to your ZX Next or N-GO hardware.  There *is* a risk of damage to your local filesystem.  **In particular you should take care selecting the target deploy location**.  **The Wizard will delete the contents of your target location** in preparation for the new image. If you try to deploy to the root of your C Drive, you will likely not appreciate the outcome.  You, and only you, are responsible for ensuring you are comfortable using this wizard, and for its safe usage.
2. Python is not a programming language I frequently use.  Much of the code here is likely to be non-idiomatic and even ugly to fluent Python programmers.

Licence
-------

Honestly, I don't really mind what you do with this.  So long as you don't try to pass any of my work off as yours or anyone else's.
