Offline CP/M archive folder
===========================

Drop CP/M archives in this folder to make them available offline.

Archives may take two forms:

1. **Zip archives**. Each archive MUST be in the zip file format and MUST have a `.zip` file extension.  The zip should directly contain the CP/M files (*.com and other files).
2. **Folders**. The folder may have any name and should directly contain the CP/M files (*.com and other files).

Archives provided in this way should appear in the CP/M menu page of the wizard.

The recommended archive can be download from [here](http://www.cpm.z80.de/download/cpm3bin_unix.zip).
