from lib import const
from lib import screen
from lib import workbench
from lib import distros 
from lib import cpm
from lib import zxdb_dl
from lib import overlays
from lib import removals
from lib import target
from lib.exceptions import UserAbort
import curses

def main(scr):
    has_rms = len(removals.list()) > 0
    ovs = overlays.list_overlays()
    plan = workbench.BuildPlan()

    page_count = 5
    if has_rms: page_count += 1
    page_count += len(ovs)

    screen.init(scr, page_count)

    page_num = 1
    
    plan.add_distro(distros.choose_source(page_num))
    page_num += 1

    plan.add_cpm(cpm.choose_source(page_num))
    page_num += 1

    plan.add_zxdb_dl(zxdb_dl.choose_source(page_num))
    page_num += 1

    if has_rms:
        plan.add_removals(removals.choose_path_lists(page_num))
        page_num += 1
    else:
        plan.add_removals([])

    for ov in ovs:
        plan.add_overlay(ov, overlays.choose_sources(ov, page_num))
        page_num += 1

    plan.add_target(target.choose_target(page_num))
    page_num += 1

    return workbench.execute(plan, page_num)


const.init(__file__)
try:
    image = curses.wrapper(main)
    print(image)
except UserAbort as ex:
    print("User aborted")
    exit(1)
