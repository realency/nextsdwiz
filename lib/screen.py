import curses

SCHEME_YELLOW = 1
SCHEME_CYAN = 2
SCHEME_GREEN = 3
SCHEME_BLUE = 4
SCHEME_MAGENTA = 5
SCHEME_RED = 6

_SCHEME_ERROR = SCHEME_RED
_PAGE_WIDTH = 100
_PAGE_HEIGHT = 25
_PAGE_NUM_LINE = 2
_PAGE_TITLE_LINE = 4
_ERROR_LINE = _PAGE_HEIGHT - 3
_INPUT_LINE = _ERROR_LINE - 2
_CONTENT_START_LINE = 6

_stdscr = None
_pad = None
_pages_total = 0

class _Page:
    def read_input(self, hint, max_length, parser=None):
        _pad.addstr(_INPUT_LINE, 2, hint)
        _pad.clrtoeol()
        self._refresh()
        curses.echo()
        curses.curs_set(1)
        win = curses.newwin(1, max_length, _INPUT_LINE, len(hint)+3)
        try:
            while True:
                input = win.getstr(0, 0, max_length).decode("utf-8")
                if not parser: return result
                try:
                    result = parser(input)
                    self._write_error("")
                    return result
                except Exception as ex:
                    self._write_error(str(ex))
        finally:
            del win
            curses.curs_set(0)
            curses.noecho()

    def _refresh(self):
        y,x = _stdscr.getmaxyx()
        y = _PAGE_HEIGHT-1 if _PAGE_HEIGHT < y else y-1
        x = _PAGE_WIDTH-1 if _PAGE_WIDTH < x else x-1
        _pad.refresh(0,0,0,0,y,x)

    def _write_error(self, message):
        _pad.addstr(_ERROR_LINE, 2, message, curses.color_pair(_SCHEME_ERROR))
        _pad.clrtoeol()
        self._refresh()

    def _clear(self):
        _pad.clear()
        _pad.addstr(0, 0, " Next SD Card Creator Wizard ".center(_PAGE_WIDTH, "="))
        _pad.addstr(_PAGE_HEIGHT-1, 0, "="*_PAGE_WIDTH)
        _pad.addstr(_PAGE_NUM_LINE,2,"Step {0} of {1}".format(self.page_num, _pages_total))
        _pad.addstr(_PAGE_TITLE_LINE,2,self.title)

    def _wait_for_key(self, accepted=None):
        while True:
            key = _pad.getch()
            self._write_error("")
            match key:
                case (10|13|curses.KEY_ENTER):
                    return '\n'
                case key if (chr(key).upper() in accepted) or (accepted == None):
                    return chr(key).upper()
                case curses.KEY_RESIZE:
                    self._refresh()
                case _:
                    self._write_error("Option not recognised")



class _Toggle:
    def __init__(self, value, label, colour, active):
        self.value = value
        self.label = label
        self.colour = colour
        self.active = active

class TogglesPage(_Page):
    def __init__(self, page_num, title):
        self.page_num = page_num
        self.title = title
        self.items = {}

    def add(self, key, value, label, colour, active=False):
        if len(key) != 1:
            raise Exception("Toggle item keys must be single-character")
        key = key.upper()
        self.items[key] = _Toggle(value, label, colour, active)
        if active:
            self.selected.append(value)

    def exec(self):
        self._clear()

        y = _CONTENT_START_LINE
        for k in self.items:
            tog = self.items[k]
            _pad.addstr(y, 4, k)
            _pad.addstr(y, 7, "[+]" if tog.active else "[ ]")
            _pad.addstr(y, 11, tog.label, tog.colour)
            tog.row = y
            y = y+1

        _pad.addstr(_INPUT_LINE, 2, "Toggle options using keys, and press enter to confirm")
        self._refresh()

        while True:
            key = self._wait_for_key(self.items)
            if key == '\n': break
            tog = self.items[key]
            tog.active = not tog.active
            _pad.addstr(tog.row, 8, "*" if tog.active else " ")
            self._refresh()

        return [self.items[k].value for k in self.items if self.items[k].active]

class _MenuItem:
    def __init__(self, value, label, colour):
        self.value = value
        self.label = label
        self.colour = colour

class MenuPage(_Page):
    def __init__(self, page_num, title):
        self.page_num = page_num
        self.title = title
        self.default_item = None
        self.items = {}

    def add(self, key, value, label, colour, default=False):
        if len(key) != 1:
            raise Exception("Menu item keys must be single-character")
        key = key.upper()
        item = _MenuItem(value, label, colour)
        self.items[key] = item
        if default:
            self.selected = key

    def exec(self):
        self._clear()
        if not self.selected:
            self.selected = list(self.items)[0]
        y = _CONTENT_START_LINE
        for k in self.items:
            item = self.items[k]
            _pad.addstr(y, 4, k)
            _pad.addstr(y, 9, item.label, curses.color_pair(item.colour))
            if k == self.selected: _pad.addstr(y, 7, "*")
            item.row = y
            y += 1

        _pad.addstr(_INPUT_LINE, 2, "Select option using keys, and press enter to confirm")
        self._refresh()

        while True:
            key = self._wait_for_key(self.items)
            if key == '\n': break
            _pad.addstr(self.items[self.selected].row, 7, " ")
            self.selected = key
            _pad.addstr(self.items[self.selected].row, 7, "*")
            self._refresh()

        return self.items[self.selected].value

class _ProgressStep:
    def __init__(self, label, included):
        self.label = label
        self.included = included

class ProgressPage(_Page):
    def __init__(self, page_num, title):
        self.page_num = page_num
        self.title = title
        self.items = {}

    def add_step(self, key, label, included):
        self.items[key] = _ProgressStep(label, included)

    def exec(self):
        self._clear()
        y = _CONTENT_START_LINE
        for k in self.items:
            item = self.items[k]
            _pad.addstr(y, 6, item.label, curses.color_pair(SCHEME_YELLOW) if item.included else curses.color_pair(SCHEME_BLUE))
            item.row = y
            y += 1
        self._refresh()

    def doing(self, key):
        item = self.items[key]
        _pad.addstr(item.row, 4, '\u25B6')
        self._refresh()

    def done(self, key):
        item = self.items[key]
        _pad.addstr(item.row, 4, '\u2713', curses.color_pair(SCHEME_GREEN))
        self._refresh()

    def failed(self, key, error):
        item = self.items[key]
        _pad.addstr(item.row, 4, '\u2717', curses.color_pair(SCHEME_RED))
        self._write_error(str(error) + ".  Press any key")
        _pad.getch()

    def log(self, message):
        _pad.addstr(_INPUT_LINE, 2, message)
        _pad.clrtoeol()
        self._refresh()

        
def init(scr, pages):
    global _stdscr
    global _pad
    global _pages_total
    _pages_total = pages
    _stdscr = scr
    _pad = curses.newpad(_PAGE_HEIGHT+1, _PAGE_WIDTH+1)
    curses.start_color()
    curses.init_pair(SCHEME_YELLOW, curses.COLOR_YELLOW, curses.COLOR_BLACK)
    curses.init_pair(SCHEME_CYAN, curses.COLOR_CYAN, curses.COLOR_BLACK)
    curses.init_pair(SCHEME_GREEN, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(SCHEME_BLUE, curses.COLOR_BLUE, curses.COLOR_BLACK)
    curses.init_pair(SCHEME_MAGENTA, curses.COLOR_MAGENTA, curses.COLOR_BLACK)
    curses.init_pair(SCHEME_RED, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.noecho()
    curses.curs_set(0)
