import os
from .screen import TogglesPage
from . import const

_REMOVALS_FOLDER = "removals"
_PAGE_TITLE = "Select items to REMOVE from image"
_EXCLUSIONS = ["README.md", ".DS_Store"]

def list():
    folder = os.path.join(const.ROOT_FOLDER, _REMOVALS_FOLDER)
    if not os.path.isdir(folder): return []
    return [f for f in os.listdir(folder) if os.path.isfile(os.path.join(folder, f)) and (f not in _EXCLUSIONS)]

def choose_path_lists(page_num):
    folder = os.path.join(const.ROOT_FOLDER, _REMOVALS_FOLDER)
    page = TogglesPage(page_num, _PAGE_TITLE)
    i = 0
    for f in list()[:10]:
        page.add(str(i), os.path.join(folder, f), f, const.GROUP_OFFLINE)
        i += 1
    return page.exec()
