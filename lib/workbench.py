import os
import shutil
from . import const
from .utils import empty_folder, hard_remove
from .screen import ProgressPage
from .sources import Source, KIND_URL, KIND_ARCHIVE

_TEMP_FOLDER = ".tmp"
_TEMP_IMAGE = "image"

class _BuildStep:
    def __init__(self, key, label):
        self.key = key
        self.label = label
    
    def active(self):
        return True

    def execute(self, temp, image, page):
        raise NotImplementedError("execute not implemented")

class _PrepTempStep(_BuildStep):
    def __init__(self):
        _BuildStep.__init__(self, "setup", "Prepare temporary working area")

    def execute(self, temp, image, page):
        page.log("Creating temporary files folder: {0}".format(temp))
        shutil.rmtree(temp, True)
        os.mkdir(temp)
        page.log("Creating temporary image folder: {0}".format(image))
        shutil.rmtree(image, True)
        os.mkdir(image)

class _InstallPackageStep(_BuildStep):
    def __init__(self, name, label, source, applicator):
        _BuildStep.__init__(self, name, label)
        self.source = source
        self.apply = applicator

    def active(self):
        return bool(self.source)

    def execute(self, temp, image, page):
        source = self.source
        if source.kind == KIND_URL:
            page.log("Downloading {0}".format(source.location))
            source = source.download(temp, self.key)

        if source.kind == KIND_ARCHIVE:
            page.log("Unpacking {0}".format(source.location))
            source = source.unpack(temp, self.key+".unpacked")

        page.log("Moving {0} files to temporary image location".format(self.key))
        self.apply(source.location, image)

class _RemovalsStep(_BuildStep):
    def __init__(self, sources):
        _BuildStep.__init__(self, "removals", "Remove unwanted files")
        self.sources = sources

    def active(self):
        return bool(self.sources)

    def _get_file_removals(self):
        result = []
        for r in self.sources:
            with open(r) as f:
                result.extend([ln.strip() for ln in f.readlines()])
        return result

    def execute(self, temp, image, page):
        for r in self._get_file_removals():
            page.log("Deleting {0}".format(r))
            hard_remove(os.path.join(image, r))

class _OverlayStep(_BuildStep):
    def __init__(self, name, sources):
        _BuildStep.__init__(self, "overlay.{0}".format(name), "Add {0}".format(name))
        self.sources = sources

    def active(self):
        return bool(self.sources)

    def execute(self, temp, image, page):
        for source in self.sources:
            page.log("Adding {0} package {1} to temporary image location".format(self.key, source.display_name))
            if source.kind == KIND_ARCHIVE:
                source = source.unpack(temp, source.name+".unpacked")
            source.copy(image)

class _DeployStep(_BuildStep):
    def __init__(self, target):
        _BuildStep.__init__(self, "deploy", "Move image to target location")
        self.target = target

    def execute(self, temp, image, page):
        page.log("Preparing target location: {0}".format(self.target))
        empty_folder(self.target)
        page.log("Moving files to target location: {0}".format(self.target))
        shutil.copytree(image, self.target, dirs_exist_ok=True)

class _CleanUpStep(_BuildStep):
    def __init__(self):
        _BuildStep.__init__(self, "cleanup", "Clean up temporary files")

    def execute(self, temp, image, page):
        page.log("Removing temporary image folder: {0}".format(image))
        shutil.rmtree(image)
        page.log("Removing other temporary files folder: {0}".format(temp))
        shutil.rmtree(temp)        

class BuildPlan:
    def __init__(self):
        self.overlays = []
        self.prep = _PrepTempStep()
        self.clean = _CleanUpStep()

    def add_distro(self, source):
        def apply(origin, destination):
            contents = os.listdir(origin)
            if len(contents) == 1 and os.path.isdir(sub := os.path.join(origin, contents[0])): origin = sub

            if not os.path.isfile(os.path.join(origin, "TBBLUE.TBU")):
                raise Exception("Source does not appear to be a Next SD image")
            if not os.path.isdir(os.path.join(origin, "nextzxos")):
                raise Exception("Source does not appear to be a Next SD image")
          
            Source(origin).copy(destination)

        self.distro = _InstallPackageStep("distro", "Unpack ZX Next SD Image", source, apply)

    def add_cpm(self, source):
        def apply(origin, destination):
            if not os.path.isfile(os.path.join(origin, "ccp.com")):
                raise Exception("Source does not appear to be a CP/M image")

            Source(origin).copy(destination, "nextzxos", "cpm")

        self.cpm = _InstallPackageStep("cpm", "Unpack CP/M", source, apply)

    def add_zxdb_dl(self, source):
        def apply(origin, destination):
            if not os.path.isdir(os.path.join(origin, "dot")):
                raise Exception("Source does not appear to be a ZXDB-DL image")
            if not os.path.isdir(os.path.join(origin, "zxdb-dl")):
                raise Exception("Source does not appear to be a ZXDB-DL image")

            Source(origin, "dot").copy(destination, "dot")
            Source(origin, "zxdb-dl").copy(destination, "zxdb-dl")

        self.zxdb_dl = _InstallPackageStep("zxdb-dl", "Unpack ZXDB-Downloader", source, apply)

    def add_removals(self, sources):
        self.removals = _RemovalsStep(sources)

    def add_overlay(self, name, sources):
        self.overlays.append(_OverlayStep(name, sources))

    def add_target(self, target):
        self.deploy = _DeployStep(target)

    class __Iter:
        def __init__(self, plan):
            self.index = -1
            self.plan = plan

        def __next__(self):
            self.index += 1
            match self.index:
                case 0: return self.plan.prep
                case 1: return self.plan.distro
                case 2: return self.plan.cpm
                case 3: return self.plan.zxdb_dl
                case 4: return self.plan.removals
                case i if (i >= 5) and (i < 5+len(self.plan.overlays)): return self.plan.overlays[i-5]
                case i if (i == 5+len(self.plan.overlays)): return self.plan.deploy
                case i if (i == 6+len(self.plan.overlays)): return self.plan.clean
                case _: raise StopIteration 

    def __iter__(self):
        return BuildPlan.__Iter(self)


def execute(plan, page_num):
    temp = os.path.join(const.ROOT_FOLDER, _TEMP_FOLDER)
    image = os.path.join(temp, _TEMP_IMAGE)  # Use a temporary folder to build the image.  So that we can leave any overwrites of the actual target until everything else has gone to plan
    
    page = ProgressPage(page_num, "Building ...")

    for step in plan:
        page.add_step(step.key, step.label, step.active())

    page.exec()

    for step in plan:
        if not step.active(): continue
        page.doing(step.key)
        try:
            step.execute(temp, image, page)
        except Exception as ex:
            page.failed(step.key, str(ex))
            raise
        page.done(step.key)

    return plan.deploy.target
