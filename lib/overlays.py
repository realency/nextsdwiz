import os
from .screen import TogglesPage
from . import const
from .sources import list_sources, Source


_PAGE_TITLE_FORMAT = "Select {0} packages"
_OVERLAYS_FOLDER = "overlays"

def list_overlays():
    folder = os.path.join(const.ROOT_FOLDER, _OVERLAYS_FOLDER)
    if not os.path.isdir(folder): return []

    return [f for f in os.listdir(folder) if os.path.isdir(os.path.join(folder, f)) and len(list_sources(os.path.join(folder, f))) > 0]


def choose_sources(overlay, page_num):
    sources = list_sources(const.ROOT_FOLDER, _OVERLAYS_FOLDER, overlay)[:10]
    page = TogglesPage(page_num, _PAGE_TITLE_FORMAT.format(overlay))
    i = 0
    for s in sources:
        page.add(str(i), s, s.display_name, const.GROUP_OFFLINE)
        i += 1
        
    return page.exec()
