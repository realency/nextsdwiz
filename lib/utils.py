# utils.py
# 
# A module containing a few utility functions.
#
# I like to put some functions into a separate utils module in order to declutter the calling module.
# Functions do not need to be reused in multiple places to be considered for inclusion here.
# Functions *should* be considered for inclusion here if:
# 1.  It is possible to reason about their behaviour independently of the calling context.
# 2.  They are stand-alone and not dependent on other functions.
# 3.  It is relatively easy to infer their intent from their names - they do not perform complex operations.

import os
import shutil
import urllib.request

def hard_remove(path):
    if not os.path.exists(path):
        return
    if os.path.isfile(path):
        os.remove(path)
        return
    if os.path.isdir(path):
        shutil.rmtree(path, True)

# Readies an empty folder
#   Creates a new folder if it does not already exist
#   or removes the contents of an existing folder if it does - without removing the folder itself
def empty_folder(path):
    if os.path.isfile(path):
        raise Exception("Target already exists as a file")

    os.makedirs(path, exist_ok=True)
    for p in [os.path.join(path, f) for f in os.listdir(path)]:
        hard_remove(p)


def download_archive(url, temp, basename):
    archive = "{0}.zip".format(os.path.join(temp, basename))
    urllib.request.urlretrieve(url, archive)
    return archive
