import os
from urllib.request import urlretrieve
import shutil
from zipfile import ZipFile

import validators

KIND_FOLDER = "folder"
KIND_ARCHIVE = "archive"
KIND_URL = "url"

class Source:
    def __init__(self, path, *paths):
        if (len(paths) == 0) and validators.url(path):
            self.location = path
            self.kind = KIND_URL
            return

        self.location = os.path.join(path, *paths)
        if not os.path.exists(self.location):
            raise Exception("Source does not exist at given path", self.location)

        if os.path.isdir(self.location):
            self.kind = KIND_FOLDER
        elif _is_archive(self.location):
            self.kind = KIND_ARCHIVE
        else:
            raise Exception("Source does not appear to be a url, folder or archive")

        self.display_name = os.path.basename(self.location) if self.kind == KIND_FOLDER else os.path.basename(self.location)[:-4]

    def download(self, path, *paths):
        archive = os.path.join(path, *paths)
        if archive[-4:].lower() != ".zip": archive += ".zip"
        urlretrieve(self.location, archive)
        return Source(archive)

    def unpack(self, path, *paths):
        target = os.path.join(path, *paths)
        if not os.path.exists(target): os.mkdir(target)
        with ZipFile(self.location, 'r') as z:
            z.testzip()
            z.extractall(target)
        return Source(target)

    def copy(self, path, *paths):
        shutil.copytree(self.location, os.path.join(path, *paths), dirs_exist_ok=True)


def _is_archive(path, *paths):
    candidate = os.path.join(path, *paths)
    return candidate[-4:].lower() == ".zip" and os.path.isfile(candidate)

def list_sources(path, *paths):
    folder = os.path.join(path, *paths)
    return [Source(folder, s) for s in os.listdir(folder) if os.path.isdir(os.path.join(folder, s)) or _is_archive(folder, s)]
    
