import os
from . import const
from .exceptions import UserAbort
from .screen import MenuPage
from .sources import Source, list_sources


_PAGE_TITLE = "Select base SD card image (Spec Next distro)"
_DISTRO_IMAGES_FOLDER = "distros"
_LOCAL_PLACEHOLDER_VALUE = "#Local"
_ABORT_PLACEHOLDER_VALUE = "#Abort"

_FIXED_OPTIONS = [
    ("D",   Source("https://gitlab.com/thesmog358/tbblue/-/archive/master/tbblue-master.zip"),  "Download latest from GitLab",          const.GROUP_DOWNLOAD),
    ("L",   _LOCAL_PLACEHOLDER_VALUE,                                                           "URL, local folder, or local zip file", const.GROUP_LOCAL),
    ("X",   _ABORT_PLACEHOLDER_VALUE,                                                           "Abort",                                const.GROUP_IGNORE)
]

_DEFAULT_OPTION = "D"

def choose_source(page_num):
    sources = list_sources(const.ROOT_FOLDER, _DISTRO_IMAGES_FOLDER)[:10]
    page = MenuPage(page_num, _PAGE_TITLE)

    i = 0
    for s in sources:
        page.add(key := str(i), s, s.display_name, const.GROUP_OFFLINE, key == _DEFAULT_OPTION)
        i += 1

    for f in _FIXED_OPTIONS:
        page.add(f[0], f[1], f[2], f[3], f[0] == _DEFAULT_OPTION)
    
    result =  page.exec()

    if result == _LOCAL_PLACEHOLDER_VALUE:
        def to_source(path):
            return Source(os.getcwd(), path)
        return page.read_input("Path:", 100, to_source)
    
    if result == _ABORT_PLACEHOLDER_VALUE:
        raise UserAbort()

    return result
