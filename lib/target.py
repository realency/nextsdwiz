import os
from .screen import MenuPage
from .exceptions import UserAbort
from . import const


_PAGE_TITLE = "Select target for SD image build"
_IMAGE_FOLDER = "image"
_LOCAL_PLACEHOLDER_VALUE = "#Local"
_ABORT_PLACEHOLDER_VALUE = "#Abort"

_FIXED_OPTIONS = [
    ("C",   os.getcwd(),                                "Current working directory",    const.GROUP_LOCAL),
    ("I",   os.path.join(os.getcwd(), _IMAGE_FOLDER),   "Image subdirectory",           const.GROUP_LOCAL),
    ("D",   "D:\\",                                     "D Drive",                      const.GROUP_LOCAL),
    ("F",   _LOCAL_PLACEHOLDER_VALUE,                   "Other local folder",           const.GROUP_LOCAL),
    ("X",   _ABORT_PLACEHOLDER_VALUE,                   "Abort",                        const.GROUP_IGNORE)
]

_DEFAULT_OPTION = "I"

def choose_target(page_num):
    page = MenuPage(page_num, _PAGE_TITLE)
    for f in _FIXED_OPTIONS:
        page.add(f[0], f[1], f[2], f[3], _DEFAULT_OPTION == f[0])
    
    result = page.exec()
    
    if result == _LOCAL_PLACEHOLDER_VALUE:
        def validate(p):
            if os.path.isfile(p):
                raise Exception("{0} exists as a file".format(p))
            return p
        return page.read_input("Folder:", 100, validate)
    
    if result == _ABORT_PLACEHOLDER_VALUE:
        raise UserAbort()

    return result
