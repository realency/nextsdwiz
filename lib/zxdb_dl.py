import os
from .screen import MenuPage
from . import const
from .sources import list_sources, Source

_PAGE_TITLE = "Select zxdb-downloader image"
_ZXDB_DL_IMAGES_FOLDER = "zxdb-dl"
_LOCAL_PLACEHOLDER_VALUE = "#Local"
_SKIP_PLACEHOLDER_VALUE = "#Skip"

_FIXED_OPTIONS = [
    ("D",   Source("https://github.com/em00k/next-zxdb-downloader/releases/download/v0.96/next-zxdb-downloader.zip"),   "Download v0.96 from GitHub",               const.GROUP_DOWNLOAD),
    ("L",   _LOCAL_PLACEHOLDER_VALUE,                                                                                   "URL, local folder, or localzip file",      const.GROUP_LOCAL),
    ("X",   _SKIP_PLACEHOLDER_VALUE,                                                                                    "Skip (do not include zxdb-downloader)",    const.GROUP_IGNORE)
]

_DEFAULT_OPTION = "D"

def choose_source(page_num):
    sources = list_sources(const.ROOT_FOLDER, _ZXDB_DL_IMAGES_FOLDER)
    page = MenuPage(page_num, _PAGE_TITLE)
    i = 0
    for s in sources:
        page.add(key := str(i), s, s.display_name, const.GROUP_OFFLINE, key == _DEFAULT_OPTION)
        i += 1
    
    for f in _FIXED_OPTIONS:
        page.add(f[0], f[1], f[2], f[3], f[0] == _DEFAULT_OPTION)
    
    result =  page.exec()

    if result == _LOCAL_PLACEHOLDER_VALUE:
        def to_source(path):
            return Source(os.getcwd(), path)
        return page.read_input("Path:", 100, to_source)
    
    if result == _SKIP_PLACEHOLDER_VALUE:
        return None

    return result
