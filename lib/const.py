import os
from . import screen

# Mapping item groups to colour schema
GROUP_OFFLINE = screen.SCHEME_YELLOW
GROUP_DOWNLOAD = screen.SCHEME_CYAN
GROUP_LOCAL = screen.SCHEME_GREEN
GROUP_IGNORE = screen.SCHEME_MAGENTA

ROOT_FOLDER = None

def init(entry_point): 
    global ROOT_FOLDER
    ROOT_FOLDER = os.path.dirname(os.path.abspath(entry_point))
